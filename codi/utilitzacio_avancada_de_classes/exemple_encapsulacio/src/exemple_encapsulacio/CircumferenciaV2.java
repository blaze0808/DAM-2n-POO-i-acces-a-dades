package exemple_encapsulacio;

/**
 *
 * Molt similar a la versió 1. S'ha canviat la implementació
 * de l'àrea. Com que s'ha considerat que calcular l'àrea era
 * una operació costosa, només es farà en cas que es necessiti.
 *
 */
public class CircumferenciaV2 {
	private double area = -1;
	public final double radi;
	public final double centreX;
	public final double centreY;

	public CircumferenciaV2(double radi) {
		this(0,0,radi);
	}

	public CircumferenciaV2(double centreX, double centreY) {
		this(centreX,centreY,1);
	}

	public CircumferenciaV2(double centreX, double centreY, double radi) {
		this.centreX = centreX;
		this.centreY = centreY;
		this.radi = radi;
	}

	public double getArea() {
		if (area < 0) {
			area = Math.PI * radi * radi;
		}
		return area;
	}
}
