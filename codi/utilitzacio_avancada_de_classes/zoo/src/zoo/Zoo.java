package zoo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Zoo {
	static final Random r = new Random();
	private List<Animal> animals = new ArrayList<Animal>();
	private Espectador espectador = new Espectador();
	
	public Animal mostraAnimal() {
		Animal a = null;
		
		if (animals.size() > 0)
			a = animals.get(r.nextInt(animals.size()));
		return a;
	}
	
	public void afegeixAnimal(String s) {
		if (s.equals("vaca")) {
			afegeixAnimal(new Vaca());
		} else if (s.equals("cocodril")) {
			afegeixAnimal(new Cocodril());
		}
	}
	
	private void afegeixAnimal(Animal a) {
		animals.add(a);
	}
	
	public void suprimeixAnimal(String s) {
		boolean surt = false;
		Iterator<Animal> it = animals.iterator();
		
		if (s.equals("vaca")) {
			while (!surt && it.hasNext()) {
				if (it.next() instanceof Vaca) {
					it.remove();
					surt = true;
				}
			}
		} else if (s.equals("cocodril")) {
			while (!surt && it.hasNext()) {
				if (it.next() instanceof Cocodril) {
					it.remove();
					surt = true;
				}
			}
		}
	}
	
	public void suprimeixAnimal(Animal a) {
		animals.remove(a);
	}
	
	public void suprimeixTots(String s) {
		Iterator<Animal> it = animals.iterator();
		
		if (s.equals("vaca")) {
			while (it.hasNext()) {
				if (it.next() instanceof Vaca)
					it.remove();
			}
		} else if (s.equals("cocodril")) {
			while (it.hasNext()) {
				if (it.next() instanceof Cocodril)
					it.remove();
			}
		}
	}
	
	public String mira() {
		String s;
		int num = r.nextInt(2);
		if (num==0)
			s = espectador.accio(this);
		else {
			Animal a = mostraAnimal();
			if (a==null)
				s = "Quin zoo més avorrit! No té cap animal";
			else
				s = a.accio(this);
		}
		return s;
	}
		
	@Override
	public String toString() {
		String s="";
		
		for (Animal a : animals) {
			if (a instanceof Vaca)
				s+="V ";
			else if (a instanceof Cocodril)
				s+="C ";
			else
				s+="? ";
		}
		return s;
	}
}