package zoo;

import java.util.Scanner;

public class PrincipalZoo {
	private Zoo zoo = new Zoo();
	
	public static void main(String[] args) {
		new PrincipalZoo();
	}
	
	private void afegeix(String nomAnimal) {
		zoo.afegeixAnimal(nomAnimal);
		if (nomAnimal.equals("vaca"))
			System.out.println("Ha arribat al zoo una enorme vaca");
		else if (nomAnimal.equals("cocodril"))
			System.out.println("Ha arribat al zoo un perillós cocodril");
	}
	
	private void suprimeix(String nomAnimal) {
		zoo.suprimeixAnimal(nomAnimal);
		if (nomAnimal.equals("vaca"))
			System.out.println("S'ha traslladat una vaca a un altre zoo");
		else if (nomAnimal.equals("cocodril"))
			System.out.println("S'ha traslladat un cocodril a un altre zoo");
	}
	
	private void suprimeixTots(String nomAnimal) {
		zoo.suprimeixTots(nomAnimal);
		if (nomAnimal.equals("vaca"))
			System.out.println("El zoo deixa de tenir vaques");
		else if (nomAnimal.equals("cocodril"))
			System.out.println("El zoo deixa de tenir cocodrils");
	}
	
	public PrincipalZoo() {
		String ordre = "";
		String[] parts;
		Scanner sc = new Scanner(System.in);
		System.out.println("El zoo obre les seves portes");
		do {
			ordre = sc.nextLine();
			parts = ordre.split(" ");
			if (parts.length == 1) {
				if (parts[0].equals("mira")) {
					System.out.println(zoo.mira());
				}
			} else if (parts.length == 2) {
				if (parts[0].equals("afegeix")) {
					afegeix(parts[1]);
				}
				if (parts[0].equals("suprimeix")) {
					suprimeix(parts[1]);
				}
			} else if (parts.length == 3) {
				if (parts[0].equals("suprimeix") && parts[1].equals("tots")) {
					suprimeixTots(parts[2]);
				}
			}
			//System.out.println(zoo.toString()); // Descomenta per veure el zoo sencer
		} while (!ordre.equals("surt"));
		sc.close();
	}

}