package ingredients;

import base.Ingredient;
import base.IngredientExtra;

public class Formatge extends IngredientExtra implements Cloneable {
	public Formatge(Ingredient base) {
		super(base);
	}
	@Override
	public String recepta() {
		String rec = super.recepta() + "\n";
		return rec + "El cuiner escampa formatge ratllat per sobre.";
	}
	@Override
	public double getPreu() {
		double preu = super.getPreu();
		return preu + 2.5;
	}
	@Override
	public String descripcio() {
		String desc = super.descripcio()+", ";
		return desc + "formatge ratllat";
	}
	@Override
	public Formatge clone() throws CloneNotSupportedException {
		return (Formatge) super.clone();
	}
}
