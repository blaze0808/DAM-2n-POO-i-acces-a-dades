package modificacio_dades;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex1 {
	private static Scanner scanner = new Scanner(System.in);
	private static String url = "jdbc:mysql://localhost:3306/onomastica";
	private static String user = "root";
	private static String passwd = "super3";

	public static void main(String[] args) {
		new Ex1().run("noms_nascuts_2012.csv");
	}
	
	public List<Name> readNames(String filename) throws IOException {
		String line;
		List<Name> names = new ArrayList<Name>();
		try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
			while ((line=reader.readLine())!=null) {
				names.add(Name.parseName(line));
			}
		}
		return names;
	}
	
	public void run(String filename) {
		long endTime;
		int option = menu();
		long initialTime = System.currentTimeMillis();
		try {
			switch (option) {
			case 1:
				insereix1(readNames(filename));
				break;
			case 2:
				insereix2(readNames(filename));
				break;
			case 3:
				insereix3(readNames(filename));
				break;
			case 4:
				esborra();
				break;
			}
			endTime = System.currentTimeMillis();
			System.out.println("Temps d'execució: "+(endTime-initialTime)+" mil·lisegons.");
		} catch (SQLException | IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void insereix1(List<Name> names) throws SQLException {
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement()) {
			for (Name name : names) {
				String sql = "INSERT INTO noms2012 VALUES ("+name.position+",'"+
						name.name+"','"+name.sex+"',"+name.freq+","+name.percentage+")";
				int nRows = statement.executeUpdate(sql);
				if (nRows!=1)
					throw new SQLException("Error en línia "+sql);
			}
		}
	}
	
	public void insereix2(List<Name> names) throws SQLException {
		String sql = "INSERT INTO noms2012 VALUES (?,?,?,?,?)";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				PreparedStatement statement = connexio.prepareStatement(sql);) {
			for (Name name : names) {
				statement.setInt(1, name.position);
				statement.setString(2, name.name);
				statement.setString(3, name.sex+"");
				statement.setInt(4, name.freq);
				statement.setDouble(5, name.percentage);
				statement.executeUpdate();
			}
		}
	}
	
	public void insereix3(List<Name> names) throws SQLException {
		boolean first = true;
		StringBuilder sql =
				new StringBuilder("INSERT INTO noms2012 VALUES ");
		for (Name name : names) {
			if (first) {
				first = false;
			} else {
				sql.append(",");
			}
			sql.append("(").append(name.position).append(",'").append(name.name).append("','")
				.append(name.sex).append("',").append(name.freq).append(",")
				.append(name.percentage).append(")");
		}
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement()) {
			statement.executeUpdate(sql.toString());
		}
	}
	
	public void esborra() throws SQLException {
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement()) {
			statement.executeUpdate("TRUNCATE noms2012");
		}
	}

	public int menu() {
		boolean ok = false;
		int option=0;
		System.out.println("1- Inserció amb molts Statement.");
		System.out.println("2- Inserció amb molts PreparedStatement.");
		System.out.println("3- Inserció amb un únic Statement.");
		System.out.println("4- Esborra la taula.");
		while (!ok) {
			System.out.println("Tria l'opció:");
			if (scanner.hasNextInt()) {
				option=scanner.nextInt();
				if (option>=1 && option<=4)
					ok=true;
			}
			scanner.nextLine();
		}
		return option;
	}
}
