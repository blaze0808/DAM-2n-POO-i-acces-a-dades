package consultes_parametres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/*
3- Fes un programa que permeti cercar un client i ens mostri tots els pagaments
que ha fet entre dues dates donades. De cada pagament es mostrarà la
quantitat, la data, i el nom de l'ítem que s'havia llogat (si és el cas,
hi ha pagaments que no estan associats a cap ítem).
 */
public class Ex3 {
	private static final String url = "jdbc:mysql://localhost:3306/sakila";
	private static final String user = "root";
	private static final String pass = "usbw";
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String nomClient = "";
		String cognomClient = "";
		String dataInicial = "";
		String dataFinal = "";
		boolean hiHaResultats = false;
		String sql = "SELECT p.amount, p.payment_date, f.title " +
					"FROM payment p " +
					"LEFT JOIN rental r ON p.rental_id=r.rental_id "+
					"JOIN inventory i USING(inventory_id) "+
					"JOIN film f ON f.film_id = i.film_id "+
					"JOIN customer c ON c.customer_id = r.customer_id "+ 
					"LEFT JOIN (customer,rental,inventory,film) " +
					"WHERE c.first_name LIKE ? " +
					"AND c.last_name LIKE ? " +
					"AND p.payment_date BETWEEN ? AND ?";
		try (Connection connexio = DriverManager.getConnection(url,user,pass);
				PreparedStatement statement = connexio.prepareStatement(sql)) {
			System.out.print("Escriu el nom d'un client per buscar totes els ítems en lloger: ");
			nomClient = entrada.nextLine();
			System.out.print("Escriu el cognom del client: ");
			cognomClient = entrada.nextLine();
			System.out.print("Escriu la data inicial en format <YYYY-MM-DD>: ");
			dataInicial = entrada.nextLine();
			System.out.print("Escriu la data final en format <YYYY-MM-DD>: ");
			dataFinal = entrada.nextLine();
			statement.setString(1, nomClient);
			statement.setString(2, cognomClient);
			statement.setString(3, dataInicial);
			statement.setString(4, dataFinal);
			try (ResultSet rs = statement.executeQuery()) {
				while(rs.next()) {
					if(!hiHaResultats) {
						hiHaResultats = true;
						System.out.println("Pagaments que té el client " + nomClient + " " + cognomClient + " entre les dates " + dataInicial + " - " + dataFinal + 
								"\n-----------------------------------------------------------------");
					}
					System.out.println("Quantitat: " + rs.getString(1) + "\t\tData pagament: " + rs.getString(2) + "\t\tTítol Pel·lícula: " + rs.getString(3));
				}
				if(!hiHaResultats)
					System.out.println("No s'ha trobat el client, o no té cap pagament en aquestes dates");
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		entrada.close();
	}
}
