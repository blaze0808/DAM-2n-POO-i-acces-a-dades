### Jerarquia de classes

#### La classe Object

**Object** és la classe que es troba a dalt de tot en la jerarquia de
classes. Directa o indirectament, totes les altres classes deriven
d'*Object*, i hereten per tant els seus mètodes.

En algunes circumstàncies pot ser interessant sobreescriure alguns dels
mètodes de la classe *Object*:

* ```protected Object clone() throws CloneNotSupportedException```

Aquest mètode ja s'ha vist a l'apartat d'interfícies. És *protected* per
evitar que es pugui cridar des de qualsevol lloc. Les classes que
implementen *Cloneable* el fan públic.

* ```public boolean equals(Object obj)```

Per comprovar la igualtat entre dos objectes. En la implementació
d'*Object*, aquest mètode és equivalent a l'operador ==, que compara si
dues referències apunten al mateix objecte, però es pot sobreescriure
per permetre altres formes de comparació. Un cas notable d'això és a la
classe *String*, on el mètode *equals* comprova que dues cadenes siguin
iguals, no que siguin la mateixa.

Sobreescriure *equals* no és trivial: si es fa, cal també sobreescriure
el *hashCode(*).

* ```protected void finalize() throws Throwable```

Es crida automàticament abans d'eliminar un objecte de memòria, i es pot
sobreescriure per alliberar algun recurs del sistema que no pugui
gestionar el col·lector de brossa automàticament.

* ```public int hashCode()```

Retorna un *codi hash* per l'objecte. Un **codi hash** és un resum de
l'objecte que facilita la seva indexació en algunes estructures
dinàmiques (*HashTable*, *HashSet*). Dos objectes iguals (en el sentit
que *equals* doni *true*), han de retornar el mateix *hash*, i dos
objectes diferents, preferiblement però no obligatòriament, han de
retornar un *hash* diferent. És per aquest motiu que cal sobreescriure
aquest mètode si s'ha sobreescrit l'*equals*.

* ```public String toString()```

Retorna una representació en forma de cadena de l'objecte. Aquest mètode
se sobreescriurà gairebé sempre, per facilitar la depuració del codi i
el manteniment de registres del programa. Ha de retornar una cadena que
especifiqui tant com sigui possible la informació de l'objecte, i que
sigui fàcil de llegir. La implementació d'*Object* retorna el nom de la
classe i el codi *hash* de l'objecte.

* ```public final Class getClass()```

Retorna un objecte de tipus *Class* que fa referència al tipus d'objecte
en l'entorn d'execució actual. Els objectes de tipus *Class* es poden
utilitzar per examinar, en temps d'execució, les característiques d'una
classe. Algunes aplicacions avançades permeten carregar noves classes en
temps d'execució i incorporar-les al programa. De la capacitat d'un
programa de veure la seva estructura interna i modificar-la en temps
d'execució se n'anomena **reflection**.

***Exemple:***

```java
public class ProvaClass {
  public static void mostraInfoClasse(Object obj) {
      int i;
      Class<?>[] tipus;

      System.out.println("La classe de " + obj.toString() + " és " +
                  obj.getClass().getName());
      System.out.println("Els mètodes de la classe són:");
      for (Method metode : obj.getClass().getMethods()) {
          System.out.print(metode.getReturnType().getName()+" "+
                  metode.getName()+"(");

          tipus = metode.getParameterTypes();
          for (i=0; i<tipus.length; i++) {
              System.out.print(tipus[i].getName());
              if (i != tipus.length - 1)
                  System.out.print(", ");
          }
          System.out.println(");");
      }
  }

  public static void main(String[] args) {
      Rectangle r = new Rectangle();
      mostraInfoClasse(r);
  }
}
```

L'execució d'aquest programa ens mostrarà tots els mètode de
*Rectangle*, els seus paràmetres, i el seu valor de retorn.
