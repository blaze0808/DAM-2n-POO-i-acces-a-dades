#### Crida de constructors

Imaginem la següent estructura de classes:

![estructura de classes](../imatges/constructors_exemple_herencia.png)

Per construir un objecte de tipus *Finestra* es procedeix de la següent
manera:

1. Qui vol obtenir l'objecte crida al constructor de la classe *Finestra*
utilitzant l'operador *new*.

2. El constructor de *Finestra*, com a primera instrucció de totes, crida el
constructor de *Contenidor*.

3. El constructor de *Contenidor*, com a primera instrucció, crida el
constructor de *Component*.

4. El constructor de *Component*, com a primera instrucció, crida el constructor
d'*Object*.

5. El constructor d'*Object* inicialitza les variables definides a la classe
*Object* i retorna.

6. El constructor de *Component*, inicialitza les variables definides a
*Component* i retorna.

7. El constructor de *Contenidor*, inicialitza les variables definides a
*Contenidor* i retorna.

8. El constructor de *Finestra*, inicialitza les variables definides a
*Finestra* i retorna.

9. L'objecte es retorna del *new*.

![exemple constructors](../imatges/constructors_exemple.png)

Si s'omet la crida al constructor de la classe superior com a primera
línia en un constructor, automàticament es crida al constructor sense
paràmetres de la classe superior. És per això que no hem hagut de cridar
mai explícitament a un constructor de la classe *Object* des de cap de
les nostres classes (que si no s'especifica una altra classe, deriven
almenys d'*Object*).

Recordeu que el Java proporciona un constructor per defecte a totes les
classes, que no rep cap paràmetre, però cal tenir present que el
constructor per defecte desapareix automàticament en el moment en què
definim un constructor qualsevol, de manera que no sempre existeix un
constructor sense paràmetres.

Si el constructor sense paràmetres de la classe superior no existeix, o
bé volem utilitzar un constructor que rep algun paràmetre, cal fer la
crida al constructor de la classe superior explícitament.

Aquesta crida es fa utilitzant la paraula clau ***super*** com si fos el
nom del constructor i passant-li els paràmetres necessaris.

***Exemple:***

```java
public class ClasseBase {
   public int variablePublic;
   protected int variableProtected;
   private int variablePrivate;

   public ClasseBase(int var1, int var2) {
       // crida implícita al constructor d'Object
       // inicialització de variables de ClasseBase:
       variableProtected = var1;
       variablePrivate = var2;
   }
   // (...)
}

public class ClasseDerivada extends ClasseBase {
   private int variable;

   public ClasseDerivada(int v, int var1, int var2) {
       // no hi ha constructor sense paràmetres a ClasseBase
       // i estem obligats a cridar explícitament:
       super(var1, var2);
       // inicialització de variables de ClasseDerivada:
       variable = v;
   }

   // (...)
}
```

Podem resumir el que hem explicat amb les següents 4 regles:

 * La primera acció que fa un constructor és cridar un constructor de la classe
 mare.

 * Aquesta crida la podem fer explícita amb la paraula clau *super*.

 * Si no la fem explícita, es crida automàticament el constructor sense
 paràmetres de la classe mare.

 * Si no hi ha constructor sense paràmetres, estem obligats a cridar un
 constructor de forma explícita.
