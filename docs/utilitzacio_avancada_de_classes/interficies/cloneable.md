#### La interfície Cloneable

En aquest apartat utilitzarem la classe *Punt* pels exemples:

```java
public class Punt {
   private double x;
   private double y;

   public Punt(double x, double y) {
       this.x = x;
       this.y = y;
   }
   public double getX() {
       return x;
   }
   public void setX(double x) {
       this.x = x;
   }
   public double getY() {
       return y;
   }
   public void setY(double y) {
       this.y = y;
   }

   @Override
   public String toString() {
       return String.format("(%.1f , %.1f)", x, y);
   }
}
```

En Java, quan fem una assignació (`variable1 = variable2`)
estem copiant la referència a l'objecte, és a dir, les dues variables
apunten al mateix objecte. De vegades, però, volem crear una còpia de
l'objecte en qüestió, en comptes de simplement referenciar-lo. Per
exemple, en el mètode d'ordenació de l'apartat anterior, es reaprofita
el mateix vector que es passa per guardar-hi el vector ordenat. Si
volguéssim mantenir a més la posició dels elements en el vector
original, abans de cridar a *sort* hauríem de fer una còpia del vector.

Vegem què passa quan assignem un punt a un altre punt i en modifiquem
alguna variable:

```java
    public static void main(String[] args) {
       Punt p = new Punt(3, 6);
       Punt q = p;
       q.setX(1);
       System.out.println(p);
       System.out.println(q);
   }
```

Quan executem obtenim:

```java
(1,0 , 6,0)
(1,0 , 6,0)
```

Fixeu-vos que l'assignació a *q* ha modificat tant *p* com *q*, perquè
realment referencien el mateix objecte.

Si una classe implementa la interfície *Cloneable* significa que té
sentit fer-ne una còpia. Aquesta interfície no incorpora cap mètode,
sinó que el mètode *clone* per fer una còpia d'un objecte ja es troba
definit a *Object*. Per què? Doncs imaginem que tenim la classe
*DataLloc* de l'apartat anterior. El que no farem és definir com es
copia una data a *Data* i després tornar-ho a fer a *DataLloc*. En
comptes d'això, quan clonem un *DataLloc* es copiarà la part pròpia, el
lloc, i per clonar la data es cridarà al *clone* de *Data*. Si *Data*
deriva d'una altra classe, s'anirà cridant el *clone* de la classe
superior, fins que arribem a *Object*.

D'aquesta manera, cada objecte és responsable de copiar només la seva
part, i no hem de repetir codi (recordeu: cada objecte és responsable de
sí mateix). A més, cada objecte pot copiar les seves variables privades,
cosa que si ho intentéssim fer directament a la classe més concreta no
podríem. Per tant, és necessari garantir que totes les classes tinguin
aquest mètode, i la forma de fer-ho és definint-lo a *Object*.

Però això no farà que qualsevol pugui cridar al mètode *clone* de
qualsevol objecte, encara que no implementi *Cloneable*? Doncs no,
perquè el mètode *clone* d'*Object* és *protected*, així que un objecte
només pot cridar el mètode *clone* d'ell mateix (perquè deriva
d'*Object*) i de les seves classes superiors, però d'altres objectes.

El mètode *clone* de la classe *Object* fa dues coses. La primera és que
si cridem el mètode *clone* en un objecte la classe del qual no
implementa la interfície *Cloneable*, obtindrem una excepció de tipus
*CloneNotSupportedException*. La segona és que copia totes les variables
de l'objecte.

Així, si volem que un objecte es pugui clonar, hem de fer dues coses:
indicar-ho, implementant la interfície *Cloneable*, i sobreescriure el
mètode *clone*, posant-lo com a públic. Dins d'aquest mètode, cridarem
el *clone* de la classe superior.

Modifiquem la declaració de la classe *Punt*:

```java
public class Punt implements Cloneable
```

I afegim el mètode *clone*:

```java
    @Override
   public Punt clone() {
       try {
           return (Punt) super.clone();
       } catch (CloneNotSupportedException e) {
           // Mai ha d'arribar aquí
           e.printStackTrace();
           return null;
       }
   }
```

El nostre *clone* no fa pràcticament res, sinó que deixem que sigui el
comportament per defecte del *clone* d'*Object* qui s'encarregui de
copiar les variables *x* i *y*.

El nostre programa queda així:

```java
    public static void main(String[] args) {
       Punt p = new Punt(3, 6);
       Punt q = p.clone();
       q.setX(1);
       System.out.println(p);
       System.out.println(q);
   }
```

I la sortida per pantalla:

```java
(3,0 , 6,0)
(1,0 , 6,0)
```

Anem a complicar una mica les coses. Fem la classe *Segment* com a
composició de dos punts:

```java
public class Segment implements Cloneable {
   private Punt inici;
   private Punt fi;

   public Punt getInici() {
       return inici;
   }
   public void setInici(Punt inici) {
       this.inici = inici;
   }
   public Punt getFi() {
       return fi;
   }
   public void setFi(Punt fi) {
       this.fi = fi;
   }
   public Segment(Punt inici, Punt fi) {
       this.inici = inici;
       this.fi = fi;
   }
   @Override
   public String toString() {
       return "Segment [inici=" + inici + ", fi=" + fi + "]";
   }
   @Override
   public Segment clone() {
       try {
           return (Segment) super.clone();
       } catch (CloneNotSupportedException e) {
           // Mai ha d'arribar aquí
           e.printStackTrace();
           return null;
       }
   }
}
```

Què surt per pantalla quan executem el següent codi?

```java
    public static void main(String[] args) {
       Punt p = new Punt(3, 6);
       Punt q = new Punt(5, 9);
       Segment s = new Segment(p, q);
       Segment t = s.clone();

       t.setInici(new Punt(0, 0));
       System.out.println(s);
       System.out.println(t);

       Punt punt = t.getFi();
       punt.setX(0);
       System.out.println(s);
       System.out.println(t);
   }
```

Doncs això:

```java
Segment [inici=(3,0 , 6,0), fi=(5,0 , 9,0)]
Segment [inici=(0,0 , 0,0), fi=(5,0 , 9,0)]
Segment [inici=(3,0 , 6,0), fi=(0,0 , 9,0)]
Segment [inici=(0,0 , 0,0), fi=(0,0 , 9,0)]
```

La primera assignació ha tingut el resultat que esperàvem: modificar
l'inici del segon segment sense modificar el del primer. Però no ha
estat així en la segona assignació: en aquest cas s'han modificat els
dos segments. Per què?

Perquè s'han copiat directament les referències als punts *inici* i *fi*
a l'objecte clonat. Així, si canviem el punt per un punt nou, com en el
primer cas, tot funciona bé, però si modifiquem el punt directament,
resulten modificats tots dos segments.

En aquests casos cal modificar el comportament del *clone*, fent quelcom
similar a això:

```java
    @Override
   public Segment clone() {
       Segment nouSegment;

       try {
           nouSegment = (Segment) super.clone();
           nouSegment.inici = inici.clone();
           nouSegment.fi = fi.clone();
           return nouSegment;
       } catch (CloneNotSupportedException e) {
           // Mai ha d'arribar aquí
           e.printStackTrace();
           return null;
       }
   }
```

Amb aquesta modificació, el resultat del programa anterior és l'esperat:

```java
Segment [inici=(3,0 , 6,0), fi=(5,0 , 9,0)]
Segment [inici=(0,0 , 0,0), fi=(5,0 , 9,0)]
Segment [inici=(3,0 , 6,0), fi=(5,0 , 9,0)]
Segment [inici=(0,0 , 0,0), fi=(0,0 , 9,0)]
```

Aquest tipus de còpia, en què ens assegurem que tots els elements són
nous i cap és un referència a un objecte existent, s'anomena **còpia
profunda**.

Hi ha programadors que opinen que el sistema del *clone* resulta propens
a errors. També es poden utilitzar altres sistemes per realitzar còpies
d'objectes:

 * El **constructor per còpia**. Es tracta d'un constructor que rep un element
 del mateix tipus i retorna una còpia profunda del mateix:

```java
   public Punt(Punt p) {
       x = p.x;
       y = p.y;
   }
   public Segment(Segment s) {
       inici = new Punt(s.inici);
       fi = new Punt(s.fi);
   }
```

 * Un **factory method estàtic**. Es tracta d'un mètode estàtic de la classe que
 crea objectes com a còpies profundes dels paràmetres.
 
