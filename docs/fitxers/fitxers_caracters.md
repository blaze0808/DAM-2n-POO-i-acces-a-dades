## Lectura i escriptura de fitxers de text caràcter a caràcter

Les classes bàsiques per llegir i escriure text en Java són
*FileReader* i *FileWriter* respectivament.

### Escriptura d'un fitxer de text

Per escriure un fitxer en format text utilitzem la classe *FileWriter*. En
aquesta classe tenim diverses sobrecàrregues del mètode *write()* que permeten
escriure caràcters, cadenes senceres, o parts de cadenes.

També tenim diverses variants del mètode *append()* que funciona com el
*write()* amb una única diferència: el mètode *write()* retorna *void*, mentre
que *append()* retorna el propi *Writer* sobre el qual l'hem cridat. Això
ens permet enllaçar diverses crides a *append()*:

```java
escriptor.append(cadena1).append(cadena2).append(cadena3);
```

Per escriure qualsevol altra dada l'haurem de passar a cadena, cosa que
habitualment no és complicada.

En el següent exemple escriviu un fitxer amb algunes línies de text:

```java
public class EscriptorText {
	public static void main(String[] args) {
    String[] provincies = {"Barcelona", "Girona", "Lleida", "Tarragona"};
		String nomFitxer = "provincies.txt";
		try (FileWriter escriptor =new FileWriter(nomFitxer)) {
			for (String provincia : provincies) {
				escriptor.append(provincia).write('\n');
      }
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
```

Si volem afegir dades a un fitxer, sense esborrar el seu contingut actual,
podem cridar el constructor de *FileWriter* passant *true* com a segon
paràmetre:

```java
FileWriter escriptor =new FileWriter(nomFitxer,true)
```

El fitxer de text generat té aquest aspecte:

```
Barcelona
Girona
Lleida
Tarragona
```

### Lectura d'un fitxer de text caràcter a caràcter

Anem a utilitzar un *FileReader* per recuperar el text anterior:

```java
public class LectorText {
	public static void main(String[] args) {
		String nomFitxer = "provincies.txt";
		int lectura;
		char ch;
		StringBuilder s = new StringBuilder();
		List<String> provincies = new ArrayList<String>();
		try (FileReader lector = new FileReader(nomFitxer)) {
			while ((lectura = lector.read())!=-1) {
				ch = (char) lectura;
				if (ch == '\n') {
					provincies.add(s.toString());
					s.setLength(0);
				} else {
					s.append(ch);
				}
			}
			for (String provincia : provincies) {
				System.out.println(provincia);
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
```

En aquest exemple hi ha diversos detalls a notar:

- *FileReader* només ens permet llegir caràcter a caràcters, o un número
definit de caràcters. No té un mètode per llegir línies de text completes.

- El mètode *read()* retorna un caràcter, però en un tipus *int*. Això
permet que retorni el valor especial -1 si hem arribat a final de fitxer.
Cal que descartem primer aquest valor especial i després ja podem fer un cast
a *char* per tractar el caràcter llegit de la forma que vulguem.