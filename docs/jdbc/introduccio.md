## Introducció a JDBC

Moltes aplicacions treballen amb dades persistents que, habitualment,
s'emmagatzemen en una base de dades relacional.

El *Sistema Gestor de Bases de Dades* s'encarrega de resoldre les consultes,
assegurar la integritat de les dades, i permetre l'accés concurrent a
múltiples usuaris o instàncies de programes.

Això suposa una millora important respecte a la gestió de les dades a
través de fitxers, especialment en els casos en què tinguem una quantitat
gran de dades, amb una estructura interna més o menys complexa, i amb
accessos concurrents.

Com a programadors, necessitarem que els nostres programes puguin comunicar-se
amb el SGBD: establir una connexió, executar una sèrie de sentències de
consulta i/o modificació de les dades emmagatzemades, i finalitzar la
connexió.

Els sistemes de bases de dades més utilitzats són els relacionals, i el
llenguatge per accedir a les dades el *SQL* (*Structured Query Language*).

### ODBC - Open DataBase Connectivity

L'**ODBC** és un estàndard de connexió a bases de dades relacionals,
desenvolupat per Microsoft a principis dels 90, i convertit en un
estàndard posteriorment.

L'*ODBC* utilitza un model de tres capes per accedir a la BD des d'una
aplicació:

1. Per cada SGBD compatible amb ODBC existeix un **driver ODBC**. El driver
ofereix una sèrie de funcionalitats d'accés al SGBD: tradueix les funcions
que utilitzem en el llenguatge de programació a les ordres concretes que s'han
d'enviar al SGBD concret. Diferents drivers faran aquesta traducció a ordres
diferents del SGBD, però mantindran una interfície gairebé idèntica de cara
al llenguatge de programació.

2. Un **driver manager** s'encarregarà de mirar quins *drivers* tenim
disponibles i de configurar-los per a poder-los utilitzar. També, el
*driver manager* s'encarrega d'uniformar el procés de connexió a instàncies
diferents del SGBD (per exemple, ens permet connectar a un servidor local,
o a un servidor remot).

3. L'aplicació es comunica es comunica amb el *driver manager* per obtenir el
driver adequat i establir una connexió amb la BD desitjada. Després, utilitza
el *driver* per realitzar les seves consultes contra la BD.

### JDBC - Java Database Connectivity

Una de les tasques que fa l'ODBC és traduir els tipus de dades SQL als tipus de
dades del llenguatge de programació C.

Com que els tipus del C i els del Java són diferents, l'ODBC no és un sistema
adequat per utilitzar directament des del Java.

A més, l'OBDC utilitza una interfície de programació C i és relativament
difícil d'aprendre.

Per accedir a bases de dades relacionals, el Java proporciona les seves API
pròpies com a part de l'edició estàndard, cosa que permet tenir una
solució purament Java.

El JDBC funciona de manera similar a l'ODBC: existeixen *drivers* JDBC per a
cadascun dels SGBD habituals i els utilitzem des de la nostra aplicació per
accedir-hi.

A més, existeixen ponts ODBC-JDBC i a l'inversa, que ens permeten accedir amb
JDBC a sistemes que suportin ODBC però no JDBC.

Esencialment, utilitzarem JDBC per tres tasques:

- Establir una connexió al SGBD i BD.
- Enviar sentències SQL per tal que es processin al SGBD.
- Obtenir els resultats de les nostres sentències.
