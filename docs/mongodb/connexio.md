## Establiment d'una connexió

### Connexió bàsica

Per establir una connexió amb MongoDB utilitzem la classe *MongoClient*. A
partir del client, podem sol·licitar una base de dades i, a partir
d'aquesta, una col·lecció:

```java
public class ExempleConnexio {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("mascotes");

    // ...
    client.close();
	}
}
```

Primer de tot establim connexió amb la base de dades:

```java
MongoClient client = new MongoClient();
```

Aquest línia estableix connexió amb un servidor MongoDB que s'executi a
*localhost* i estigui escoltant pel port per defecte (el **27017**).

Després seleccionem la base de dades sobre la qual treballarem:

```java
MongoDatabase db = client.getDatabase("exemples");
```

I finalment seleccionem la col·lecció:

```java
MongoCollection<Document> coll = db.getCollection("mascotes");
```

Cal notar que MongoDB gestiona internament les excepcions que es poden
produir al connectar. Això significa que el nostre programa no pot capturar
aquestes excepcions i no podem saber només amb aquestes línies si ens
hem connectat correctament.

A més, si la base de dades o la col·lecció que hem posat no existeixen,
tampoc es produirà un error, perquè el MongoDB crearà la base de dades o
col·lecció en el moment en què inserim algun element.

Quan acabem d'utilitzar la connexió cal tancar-la amb `client.close();`

### Veure quines bases de dades i col·leccions hi ha

Podem comprovar quines bases de dades hi ha al servidor de la següent manera:

```java
MongoClient client = new MongoClient();
client.listDatabaseNames().forEach((String name)->{System.out.println(name);});
client.close();
```

Aquest codi utilitza una funció lambda. També podem fer-ho amb un iterador:

```java
MongoClient client = new MongoClient();
MongoCursor<String> cursor = client.listDatabaseNames().iterator();
while (cursor.hasNext()) {
		System.out.println(cursor.next());
}
client.close();
```

Aquí hem mostrat les bases de dades existents per pantalla, però podem
fer-ne algun tipus de procés. Per exemple, el següent mètode permet
comprovar si una base de dades existeix o no:

```java
public static boolean dbExists(MongoClient client, String dbName) {
	boolean exists = false;
  MongoCursor<String> cursor = client.listDatabaseNames().iterator();
  while (cursor.hasNext() && !exists) {
      if (cursor.next().equals(dbName))
          exists = true;
  }
  return exists;
}
```

### Connexions autenticades

Si tenim l'autenticació d'usuaris activada al servidor (totalment recomanat
en un entorn de producció), hem de crear una credencial vàlida abans de
connectar-nos:

```java
String user = "root";
String database = "exemples";
char[] password = "password".toCharArray();

MongoCredential credential = MongoCredential.createCredential(user,
		database, password);
ServerAddress serverAddress = new ServerAddress("localhost");
MongoClient client = new MongoClient(serverAddress, Arrays.asList(credential));
```

### Connexions a servidors remots

Per connectar a un servidor remot en tenim prou amb utilitzar un dels
constructors de *MongoClient* que accepten el nom/IP del servidor i el port:

```java
MongoClient client = new MongoClient("servidor.mongodb.local", 1111);
```
